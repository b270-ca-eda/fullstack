let collection = [];

function print() {
  return collection;
}

function enqueue(element) {
  collection[collection.length] = element;
  return collection;
}

function dequeue() {
  
  let firstElement  = collection[collection.length - 1];
  for (let i = 0; i < collection.length - 1; i++) {
    collection[i] = collection[i + 1];
  }
  collection.length--;
  
  let newfirstElement =[];
  newfirstElement[0]=collection[0];
  return newfirstElement;
}

function front() {
  return collection[0];
}

function size() {
  return collection.length;
}

function isEmpty() {
  return collection.length === 0;
}

module.exports = {
  print,
  enqueue,
  dequeue,
  front,
  size,
  isEmpty,
};
